from hashlib import sha256
import hmac
import threading
import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import renderers
from rest_framework.decorators import api_view, renderer_classes
from django.conf import settings
from django.http import HttpResponse
import redis
import dropbox

logger = logging.getLogger(__name__)

SUCCESS = 'success'
FAILURE = 'failure'

redis_url = 'ec2-54-163-233-146.compute-1.amazonaws.com'
redis_client = redis.from_url(redis_url)


class Root(APIView):
    def get(self, request, format=None):
        return Response({SUCCESS, 'Hit dropblog with "HTTP GET" request'})

    def post(self, request, format=None):
        return Response({SUCESS, 'Hit dropblog with "HTTP POST" request'})


class HandleChange(APIView):

    def post(self, request, format=None):
        """
        Webhook callback for dropbox
        """
        return Response({SUCCESS: 'Dropbox callback triggered this!'})

    def get(self, request, format=None):
        return Response({SUCCESS: 'Dropbox callback triggered this!'})


class WebHook(APIView):

    def get(self, request, format=None):
        return HttpResponse(request.query_params.get('challenge'))

    def post(self, request, format=None):
        logger.info(request)
        signature = request.data.get('X-Dropbox-Signature')
        logger.info(request.data)

        # TODO: create custom permission for this
        if not hmac.compare_digest(signature, hmac.new(settings.SECRET_KEY, request.data, sha256).hexdigest()):
            return 403

        # for account in json.loads(request.data)['list_folder']['accounts']:
            # TODO: replace threading with celery
            # threading.Thread(target=process_user, args=(account,)).start()
        for uid in request.data.get('delta', {}).get('users'):
            threading.Thread(target=process_user, args=(uid,)).start()
        return ''


def process_user(account):
    '''Call /files/list_folder for the given user ID and process any changes.'''

    # OAuth token for the user
    token = redis_client.hget('tokens', account)

    # cursor for the user (None the first time)
    cursor = redis_client.hget('cursors', account)

    dbx = Dropbox(token)
    has_more = True

    while has_more:
        if cursor is None:
            result = dbx.files_list_folder(path='')
        else:
            result = dbx.files_list_folder_continue(cursor)

        for entry in result.entries:
            # Ignore deleted files, folders, and non-markdown files
            if (isinstance(entry, DeletedMetadata) or
                isinstance(entry, FolderMetadata) or
                not entry.path_lower.endswith('.md')):
                continue

            # Convert to Markdown and store as <basename>.html
            _, resp = dbx.files_download(entry.path_lower)
            html = markdown(resp.content)
            dbx.files_upload(html, entry.path_lower[:-3] + '.html', mode=WriteMode('overwrite'))

        # Update cursor
        cursor = result.cursor
        redis_client.hset('cursors', account, cursor)

        # Repeat only if there's more to do
        has_more = result.has_more

