from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.Root.as_view(), name='root'),
    url(r'^change/$', views.HandleChange.as_view(), name='change'),
    url(r'^webhook/$', views.WebHook.as_view(), name='hook'),
]

