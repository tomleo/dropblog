# Dropblog

Write in markdown, sync with dropbox, view on the web.

## Publishing

Move files to `publish` folder

## Todo:

- configure webhook callback view
- oAuth with dropbox
- add styling and customization settings
- multi-user access
- custom markdown formatting


## Notes

- https://www.dropbox.com/developers/reference/webhooks#webhooks
- https://peaceful-tundra-86068.herokuapp.com/
- https://github.com/dropbox/mdwebhook
- https://www.dropbox.com/developers/apps/
- https://devcenter.heroku.com/articles/celery-heroku
- https://dashboard.heroku.com/apps/peaceful-tundra-86068/resources

## Ideas

Type Checking with [mypy](https://github.com/machinalis/mypy-django)



